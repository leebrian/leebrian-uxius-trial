﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthTextController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Renderer>().sortingOrder = 10;
        GetComponent<TextMesh>().text = "HP: " + PlayerController.playerHP.ToString() + "/100";
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
