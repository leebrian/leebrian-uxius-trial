﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour {

    public float enemyHP = 40;
    public float enemyAtkPower = 10;
    public Transform damageTxtObj;
    public Transform healthTxtObj;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (BattleFlow.whichTurn==2)
        {
            GetComponent<Animator>().SetTrigger("slimeAttacking");
            GetComponent<Transform>().position = new Vector2(-3.7f, -3.98f);
            BattleFlow.whichTurn = 1;
        }

        if(BattleFlow.damageDisplay == "y")
        {
            enemyHP -= BattleFlow.currentDamage;
            BattleFlow.damageDisplay = "n";
        }

        if(enemyHP<= 0)
        {
            Destroy(gameObject);
            SceneManager.LoadScene("Samplescene");
        }
    }

    IEnumerator EnemyReturnPosition()
    {
        Instantiate(damageTxtObj, new Vector2(-5f, -2f), damageTxtObj.rotation);
        BattleFlow.damageDisplay = "y";
        PlayerController.playerHP -= enemyAtkPower;
        Debug.Log(PlayerController.playerHP);
        yield return new WaitForSeconds(1.2f);
        GetComponent<Transform>().position = new Vector2(5f, -3.98f);
    }
}
