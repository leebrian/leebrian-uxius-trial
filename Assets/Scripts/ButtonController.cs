﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

    public Button attackButton;
    public Button fleeButton;

    public bool atkState = false;
    public bool fleeState = false;

    // Use this for initialization
    void Start () {
        attackButton = GameObject.FindGameObjectWithTag("AttackButton").GetComponent<Button>();
        fleeButton = GameObject.FindGameObjectWithTag("FleeButton").GetComponent<Button>();
    }
	
	// Update is called once per frame
	void Update () {
        attackButton.onClick.AddListener(AttackButtonPress);
        fleeButton.onClick.AddListener(FleeButtonPress);
    }

    public void AttackButtonPress()
    {
        if(atkState == false)
        {
            atkState = true;
        }else if(atkState == true)
        {
            atkState = false;
        }
    }

    public void FleeButtonPress()
    {
        if (fleeState == false)
        {
            fleeState = true;
        }
    }
}
