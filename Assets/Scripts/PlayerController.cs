﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public static float playerHP = 100;
    public static float playerMaxHP = 100;
    public Transform damageTxtObj;
    public GameObject buttonController;

    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void Update()
    {

        if ((buttonController.GetComponent<ButtonController>().atkState == true) && (BattleFlow.whichTurn == 1))
        {
            BattleFlow.currentDamage = 10;
            GetComponent<Animator>().SetTrigger("heroAttacking");
            GetComponent<Transform>().position = new Vector2(3.5f, -3.33f);
        }

        if ((buttonController.GetComponent<ButtonController>().fleeState == true) && (BattleFlow.whichTurn == 1))
        {
            SceneManager.LoadScene("Samplescene");
        }
    }

    void ReturnPosition()
    {
        Instantiate(damageTxtObj, new Vector2(4.5f, -2f), damageTxtObj.rotation);
        BattleFlow.damageDisplay = "y";
        GetComponent<Transform>().position = new Vector2(-4.8f, -3.33f);
        BattleFlow.whichTurn = 2;
    }
}